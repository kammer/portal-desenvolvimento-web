export default {
    getPessoas() {
      return {        
        headers: [
            {
                text: 'Nome',
                align: 'left',
                sortable: false,
                value: 'name'
            },
            { text: 'Data Nascimento', value: 'dt_nasc', align: 'right' },
            { text: 'CPF', value: 'cpf', align: 'right' },
            { text: 'Cidade', value: 'cidade', align: 'right' },
        ],
        pessoas:[
            {
                nome: 'Raphael Kammer',
                dt_nasc: '17/08/1999',
                cpf: '589.584.125-00',
                cidade: 'Joinville'
            },
            {
                nome: 'Maria do Bairro',
                dt_nasc: '20/05/2005',
                cpf: '622.458.555-01',
                cidade: 'São Paulo'
            },
            {
                nome: 'Paulo Cesar',
                dt_nasc: '12/10/1971',
                cpf: '998.547.252-66',
                cidade: 'Itajai'
            },
            {
                nome: 'Fernanda Santos',
                dt_nasc: '17/01/2001',
                cpf: '985.555.221-63',
                cidade: 'Rio de Janeiro'
            },
            {
                nome: 'Lucas Yamada',
                dt_nasc: '25/05/1994',
                cpf: '521.669.412-55',
                cidade: 'Pato Branco'
            },
            {
                nome: 'Gabriel Kammer',
                dt_nasc: '12/10/2000',
                cpf: '852.456.214-99',
                cidade: 'Piracicaba'
            },
      ]}
    }
  }
  